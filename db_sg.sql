-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2020 at 07:42 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sg`
--

-- --------------------------------------------------------

--
-- Table structure for table `daemons`
--

CREATE TABLE `daemons` (
  `Start` text NOT NULL,
  `Info` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gammu`
--

CREATE TABLE `gammu` (
  `Version` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gammu`
--

INSERT INTO `gammu` (`Version`) VALUES
(13);

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE `inbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ReceivingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Text` text NOT NULL,
  `SenderNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT -1,
  `TextDecoded` text NOT NULL,
  `ID` int(10) UNSIGNED NOT NULL,
  `RecipientID` text NOT NULL,
  `Processed` enum('false','true') NOT NULL DEFAULT 'false'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`UpdatedInDB`, `ReceivingDateTime`, `Text`, `SenderNumber`, `Coding`, `UDH`, `SMSCNumber`, `Class`, `TextDecoded`, `ID`, `RecipientID`, `Processed`) VALUES
('2020-01-20 09:10:30', '2020-01-20 08:51:08', 'REG SAGA', '087833468531', 'Default_No_Compression', '', '', -1, 'REG SAGA', 255, '', 'true'),
('2020-01-20 11:16:17', '2020-01-20 08:51:08', 'REG SAGA', '087833468531', 'Default_No_Compression', '', '', -1, 'REG SAGA', 256, '', 'true'),
('2020-01-20 11:19:36', '2020-01-20 08:51:08', 'REG SAGA', '087833468531', 'Default_No_Compression', '', '', -1, 'REG SAGA', 257, '', 'true'),
('2020-01-20 11:47:56', '2020-01-20 08:51:08', 'REG SAGA', '087833468531', 'Default_No_Compression', '', '', -1, 'REG SAGA', 258, '', 'true'),
('2020-01-20 11:48:44', '2020-01-20 08:51:08', 'CEK PESANAN 123', '087833468531', 'Default_No_Compression', '', '', -1, 'REG SAGA', 259, '', 'true'),
('2020-01-20 11:49:41', '2020-01-20 08:51:08', 'CEK PESANAN 123', '087833468531', 'Default_No_Compression', '', '', -1, 'REG SAGA', 260, '', 'true'),
('2020-01-20 11:54:35', '2020-01-20 08:51:08', 'CEK PESANAN 123', '087833468531', 'Default_No_Compression', '', '', -1, 'CEK PESANAN 123', 261, '', 'true'),
('2020-01-20 11:57:55', '2020-01-20 08:51:08', 'CEK PESANAN 123', '087833468531', 'Default_No_Compression', '', '', -1, 'CEK PESANAN 123', 262, '', 'true'),
('2020-01-20 11:59:27', '2020-01-20 08:51:08', 'CEK PESANAN 123', '087833468531', 'Default_No_Compression', '', '', -1, 'CEK PESANAN 123', 263, '', 'true'),
('2020-01-20 15:07:40', '2020-01-20 12:54:05', '004B', '+6289652070414', 'Default_No_Compression', '', '+628964018094', -1, 'K', 264, '', 'true'),
('2020-01-20 15:07:40', '2020-01-20 12:54:06', '00520065006700200073006100670061', '+6289652070414', 'Default_No_Compression', '', '+628964018094', -1, 'Reg saga', 265, '', 'true'),
('2020-01-20 15:10:04', '2020-01-20 15:10:04', '00430065006B00200070006500730061006E0061006E00200031003200330034003400360037', '+6289652070414', 'Default_No_Compression', '', '+628964018396', -1, 'Cek pesanan 1234467', 266, '', 'true'),
('2020-01-20 15:18:11', '2020-01-20 15:10:22', '00430065006B00200070006500730061006E0061006E0020003100320033', '+6289652070414', 'Default_No_Compression', '', '+628964018096', -1, 'Cek pesanan 123', 267, '', 'true'),
('2020-01-20 15:16:43', '2020-01-20 15:16:42', '00500065006E00670069007300690061006E0020005200700035003000300030002000730075006B007300650073002E0020004D00610073006100200061006B00740069006600200073002F0064002000300037002F00300035002F0032003000320030002E00200041006B0068006900720020006D006100730061002000740065006E006700670061006E0067002000300036002F00300036002F0032003000320030002E00200048004F0054002000530041004C004500210020006400690020002A003100320033002A00340023002000440061007000610074006B0061006E002000500061006B00650074002000480045004200410054002000680061007200670061002000480045004D00410054', '3Topup', 'Default_No_Compression', '', '+628964018092', -1, 'Pengisian Rp5000 sukses. Masa aktif s/d 07/05/2020. Akhir masa tenggang 06/06/2020. HOT SALE! di *123*4# Dapatkan Paket HEBAT harga HEMAT', 268, '', 'true'),
('2020-01-20 15:17:32', '2020-01-20 15:17:32', '0054006500720069006D00610020006B0061007300690068002C0020007400720061006E00730061006B0073006900200062006500720068006100730069006C00210020004B0061006D007500200064007000740020006D0065006E0069006B006D006100740069002000700075006C007300610020006E0065006C0070006F006E00200052007000200035002E0030003000300020006200650072006C0061006B0075002000730065006C0061006D00610020003300200068006100720069002E', '3', 'Default_No_Compression', '', '+628964018093', -1, 'Terima kasih, transaksi berhasil! Kamu dpt menikmati pulsa nelpon Rp 5.000 berlaku selama 3 hari.', 269, '', 'true');

--
-- Triggers `inbox`
--
DELIMITER $$
CREATE TRIGGER `inbox_timestamp` BEFORE INSERT ON `inbox` FOR EACH ROW BEGIN
    IF NEW.ReceivingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.ReceivingDateTime = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `outbox`
--

CREATE TABLE `outbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendBefore` time NOT NULL DEFAULT '23:59:59',
  `SendAfter` time NOT NULL DEFAULT '00:00:00',
  `Text` text DEFAULT NULL,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text DEFAULT NULL,
  `Class` int(11) DEFAULT -1,
  `TextDecoded` text NOT NULL,
  `ID` int(10) UNSIGNED NOT NULL,
  `MultiPart` enum('false','true') DEFAULT 'false',
  `RelativeValidity` int(11) DEFAULT -1,
  `SenderID` varchar(255) DEFAULT NULL,
  `SendingTimeOut` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryReport` enum('default','yes','no') DEFAULT 'default',
  `CreatorID` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Triggers `outbox`
--
DELIMITER $$
CREATE TRIGGER `outbox_timestamp` BEFORE INSERT ON `outbox` FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.SendingDateTime = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingTimeOut = '0000-00-00 00:00:00' THEN
        SET NEW.SendingTimeOut = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `outbox_multipart`
--

CREATE TABLE `outbox_multipart` (
  `Text` text DEFAULT NULL,
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text DEFAULT NULL,
  `Class` int(11) DEFAULT -1,
  `TextDecoded` text DEFAULT NULL,
  `ID` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `SequencePosition` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pbk`
--

CREATE TABLE `pbk` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT -1,
  `Name` text NOT NULL,
  `Number` text NOT NULL,
  `Foto` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pbk`
--

INSERT INTO `pbk` (`ID`, `GroupID`, `Name`, `Number`, `Foto`) VALUES
(1, 2, 'ijul', '090909090', 'assets/img/4sai-wika salim.jpg'),
(28, 3, 'lisna', '0898989898', 'assets/img/f0460009.jpg'),
(29, 8, 'Entis Sutisna', '08989898989', 'assets/img/f0460145.jpg'),
(30, 2, 'Steven', '+62838898989898', 'assets/img/10609104_453027394838895_590088178_n.jpg'),
(31, 2, 'Siti Maemunah', '08989898', 'assets/img/302984-one-piece-sabo-luffy-and-ace.jpg'),
(33, 2, 'sadsadasd', '0988098989', 'assets/img/greekalphabet2col.jpg'),
(34, 2, 'Dina', '0898989898', 'assets/img/'),
(35, 2, 'Dina', '0898989898', 'assets/img/'),
(36, 2, 'Pak Jamil', '08989898', 'assets/img/'),
(37, 2, 'Pak Jamil', '08989898', 'assets/img/');

-- --------------------------------------------------------

--
-- Table structure for table `pbk_groups`
--

CREATE TABLE `pbk_groups` (
  `NameGroup` text NOT NULL,
  `GroupID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pbk_groups`
--

INSERT INTO `pbk_groups` (`NameGroup`, `GroupID`) VALUES
('Keluarga', 2),
('Teman', 3),
('Group Nakal', 9),
('Client', 8),
('Group 2', 12);

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE `phones` (
  `ID` text NOT NULL,
  `UpdatedInDB` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TimeOut` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Send` enum('yes','no') NOT NULL DEFAULT 'no',
  `Receive` enum('yes','no') NOT NULL DEFAULT 'no',
  `IMEI` varchar(35) NOT NULL,
  `Client` text NOT NULL,
  `Battery` int(11) NOT NULL DEFAULT -1,
  `Signal` int(11) NOT NULL DEFAULT -1,
  `Sent` int(11) NOT NULL DEFAULT 0,
  `Received` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`ID`, `UpdatedInDB`, `InsertIntoDB`, `TimeOut`, `Send`, `Receive`, `IMEI`, `Client`, `Battery`, `Signal`, `Sent`, `Received`) VALUES
('', '2020-01-20 18:41:51', '2020-01-20 11:11:12', '2020-01-20 18:42:01', 'yes', 'yes', '868931004644412', 'Gammu 1.33.0, Windows Server 2007, GCC 4.7, MinGW 3.11', 0, 57, 16, 6);

--
-- Triggers `phones`
--
DELIMITER $$
CREATE TRIGGER `phones_timestamp` BEFORE INSERT ON `phones` FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.TimeOut = '0000-00-00 00:00:00' THEN
        SET NEW.TimeOut = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sentitems`
--

CREATE TABLE `sentitems` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryDateTime` timestamp NULL DEFAULT NULL,
  `Text` text NOT NULL,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT -1,
  `TextDecoded` text NOT NULL,
  `ID` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `SenderID` varchar(255) NOT NULL,
  `SequencePosition` int(11) NOT NULL DEFAULT 1,
  `Status` enum('SendingOK','SendingOKNoReport','SendingError','DeliveryOK','DeliveryFailed','DeliveryPending','DeliveryUnknown','Error') NOT NULL DEFAULT 'SendingOK',
  `StatusError` int(11) NOT NULL DEFAULT -1,
  `TPMR` int(11) NOT NULL DEFAULT -1,
  `RelativeValidity` int(11) NOT NULL DEFAULT -1,
  `CreatorID` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sentitems`
--

INSERT INTO `sentitems` (`UpdatedInDB`, `InsertIntoDB`, `SendingDateTime`, `DeliveryDateTime`, `Text`, `DestinationNumber`, `Coding`, `UDH`, `SMSCNumber`, `Class`, `TextDecoded`, `ID`, `SenderID`, `SequencePosition`, `Status`, `StatusError`, `TPMR`, `RelativeValidity`, `CreatorID`) VALUES
('2020-01-20 11:16:46', '2020-01-20 11:16:46', '2020-01-20 11:16:46', NULL, '004D00610061006600200070006500720069006E007400610068002000730061006C00610068', '087833468531', 'Default_No_Compression', '', '+6289644000001', -1, 'Maaf perintah salah', 245, '', 1, 'SendingOKNoReport', -1, 9, 255, ''),
('2020-01-20 11:19:50', '2020-01-20 11:19:50', '2020-01-20 11:19:50', NULL, '004D00610061006600200070006500720069006E007400610068002000730061006C00610068', '087833468531', 'Default_No_Compression', '', '+6289644000001', -1, 'Maaf perintah salah', 246, '', 1, 'SendingOKNoReport', -1, 10, 255, ''),
('2020-01-20 12:23:26', '2020-01-20 12:23:26', '2020-01-20 12:23:26', NULL, '0054006500720069006D00610020006B0061007300690068002000740065006C006100680020006D0065006E00640061006600740061007200200053004D00530020004700610074006500770061007900200053004100470041', '087833468531', 'Default_No_Compression', '', '+6289644000001', -1, 'Terima kasih telah mendaftar SMS Gateway SAGA', 247, '', 1, 'SendingOKNoReport', -1, 11, 255, ''),
('2020-01-20 12:23:30', '2020-01-20 12:23:30', '2020-01-20 12:23:30', NULL, '0054006500720069006D00610020006B0061007300690068002000740065006C006100680020006D0065006E00640061006600740061007200200053004D00530020004700610074006500770061007900200053004100470041', '087833468531', 'Default_No_Compression', '', '+6289644000001', -1, 'Terima kasih telah mendaftar SMS Gateway SAGA', 248, '', 1, 'SendingOKNoReport', -1, 12, 255, ''),
('2020-01-20 12:23:34', '2020-01-20 12:23:34', '2020-01-20 12:23:34', NULL, '0054006500720069006D00610020006B0061007300690068002000740065006C006100680020006D0065006E00640061006600740061007200200053004D00530020004700610074006500770061007900200053004100470041', '087833468531', 'Default_No_Compression', '', '+6289644000001', -1, 'Terima kasih telah mendaftar SMS Gateway SAGA', 249, '', 1, 'SendingOKNoReport', -1, 13, 255, ''),
('2020-01-20 12:23:37', '2020-01-20 12:23:37', '2020-01-20 12:23:37', NULL, '0050006500730061006E0061006E00200041006E0064006100200053006500640061006E006700200044006900700072006F007300650073002E00200045007300740069006D006100730069002000530065006C0065007300610069002000540061006E006700670061006C00200032003000320030002D00300031002D00320032', '087833468531', 'Default_No_Compression', '', '+6289644000001', -1, 'Pesanan Anda Sedang Diproses. Estimasi Selesai Tanggal 2020-01-22', 250, '', 1, 'SendingOKNoReport', -1, 14, 255, ''),
('2020-01-20 12:23:40', '2020-01-20 12:23:40', '2020-01-20 12:23:40', NULL, '0050006500730061006E0061006E00200041006E0064006100200053006500640061006E0067002000440061006C0061006D002000500072006F007300650073002E002000530069006C00610068006B0061006E00200041006D00620069006C00200050006100640061002000540061006E006700670061006C0020', '087833468531', 'Default_No_Compression', '', '+6289644000001', -1, 'Pesanan Anda Sedang Dalam Proses. Silahkan Ambil Pada Tanggal ', 251, '', 1, 'SendingOKNoReport', -1, 15, 255, ''),
('2020-01-20 12:23:43', '2020-01-20 12:23:43', '2020-01-20 12:23:43', NULL, '0050006500730061006E0061006E00200041006E0064006100200053006500640061006E0067002000440061006C0061006D002000500072006F007300650073002E002000530069006C00610068006B0061006E00200041006D00620069006C00200050006100640061002000540061006E006700670061006C002000320032002D00300031002D0032003000320030', '087833468531', 'Default_No_Compression', '', '+6289644000001', -1, 'Pesanan Anda Sedang Dalam Proses. Silahkan Ambil Pada Tanggal 22-01-2020', 252, '', 1, 'SendingOKNoReport', -1, 16, 255, ''),
('2020-01-20 15:08:19', '2020-01-20 15:08:19', '2020-01-20 15:08:19', NULL, '004D00610061006600200070006500720069006E007400610068002000730061006C00610068', '+6289652070414', 'Default_No_Compression', '', '+6289644000001', -1, 'Maaf perintah salah', 253, '', 1, 'SendingOKNoReport', -1, 17, 255, ''),
('2020-01-20 15:08:23', '2020-01-20 15:08:23', '2020-01-20 15:08:23', NULL, '0054006500720069006D00610020006B0061007300690068002000740065006C006100680020006D0065006E00640061006600740061007200200053004D00530020004700610074006500770061007900200053004100470041', '+6289652070414', 'Default_No_Compression', '', '+6289644000001', -1, 'Terima kasih telah mendaftar SMS Gateway SAGA', 254, '', 1, 'SendingOKNoReport', -1, 18, 255, ''),
('2020-01-20 15:10:28', '2020-01-20 15:10:28', '2020-01-20 15:10:28', NULL, '004D006100610066002C0020004E006F006D006F007200200050006500730061006E0061006E00200054006900640061006B0020005400650072006400610066007400610072', '+6289652070414', 'Default_No_Compression', '', '+6289644000001', -1, 'Maaf, Nomor Pesanan Tidak Terdaftar', 255, '', 1, 'SendingOKNoReport', -1, 19, 255, ''),
('2020-01-20 15:11:00', '2020-01-20 15:11:00', '2020-01-20 15:11:00', NULL, '004D006100610066002C0020004E006F006D006F007200200050006500730061006E0061006E00200054006900640061006B0020005400650072006400610066007400610072', '+6289652070414', 'Default_No_Compression', '', '+6289644000001', -1, 'Maaf, Nomor Pesanan Tidak Terdaftar', 256, '', 1, 'SendingError', -1, -1, 255, ''),
('2020-01-20 15:16:32', '2020-01-20 15:16:32', '2020-01-20 15:16:32', NULL, '004D006100610066002C0020004E006F006D006F007200200050006500730061006E0061006E00200054006900640061006B0020005400650072006400610066007400610072', '+6289652070414', 'Default_No_Compression', '', '+6289644000001', -1, 'Maaf, Nomor Pesanan Tidak Terdaftar', 257, '', 1, 'SendingError', -1, -1, 255, ''),
('2020-01-20 15:17:30', '2020-01-20 15:17:30', '2020-01-20 15:17:30', NULL, '004D00610061006600200070006500720069006E007400610068002000730061006C00610068', '3Topup', 'Default_No_Compression', '', '+6289644000001', -1, 'Maaf perintah salah', 258, '', 1, 'SendingError', -1, -1, 255, ''),
('2020-01-20 15:18:28', '2020-01-20 15:18:28', '2020-01-20 15:18:28', NULL, '004D00610061006600200070006500720069006E007400610068002000730061006C00610068', '3', 'Default_No_Compression', '', '+6289644000001', -1, 'Maaf perintah salah', 259, '', 1, 'SendingError', -1, -1, 255, ''),
('2020-01-20 15:18:32', '2020-01-20 15:18:32', '2020-01-20 15:18:32', NULL, '0050006500730061006E0061006E00200041006E0064006100200053006500640061006E0067002000440061006C0061006D002000500072006F007300650073002E002000530069006C00610068006B0061006E00200041006D00620069006C00200050006100640061002000540061006E006700670061006C002000320030002D00300031002D0032003000320030', '+6289652070414', 'Default_No_Compression', '', '+6289644000001', -1, 'Pesanan Anda Sedang Dalam Proses. Silahkan Ambil Pada Tanggal 20-01-2020', 260, '', 1, 'SendingOKNoReport', -1, 24, 255, '');

--
-- Triggers `sentitems`
--
DELIMITER $$
CREATE TRIGGER `sentitems_timestamp` BEFORE INSERT ON `sentitems` FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.SendingDateTime = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_autoreply`
--

CREATE TABLE `tbl_autoreply` (
  `id` int(11) NOT NULL,
  `keyword1` varchar(25) NOT NULL,
  `keyword2` varchar(25) NOT NULL,
  `result` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_autoreply`
--

INSERT INTO `tbl_autoreply` (`id`, `keyword1`, `keyword2`, `result`) VALUES
(1, 'REG', 'SAGA', 'Terima kasih telah mendaftar SMS Gateway SAGA'),
(2, 'UNREG', 'SAGA', 'Anda sudah menonaktifkan acount SAGA'),
(3, 'CEK', 'PESANAN', 'INFO PESANAN');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bahan`
--

CREATE TABLE `tbl_bahan` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(200) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_bahan`
--

INSERT INTO `tbl_bahan` (`id`, `nama`, `harga`) VALUES
(1, 'biasa', 40000),
(2, 'bamboo', 50000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jasa`
--

CREATE TABLE `tbl_jasa` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(200) NOT NULL,
  `biaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_jasa`
--

INSERT INTO `tbl_jasa` (`id`, `nama`, `biaya`) VALUES
(1, '< 10 sablon', 10000),
(2, '11-20 sablon', 9000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sablon`
--

CREATE TABLE `tbl_sablon` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` enum('diproses','selesai','') NOT NULL DEFAULT 'diproses',
  `tanggal_masuk` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `id_jasa` int(10) UNSIGNED NOT NULL,
  `biaya_jasa` int(11) NOT NULL,
  `id_bahan` int(10) UNSIGNED NOT NULL,
  `harga_bahan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_sablon`
--

INSERT INTO `tbl_sablon` (`id`, `id_user`, `status`, `tanggal_masuk`, `tanggal_selesai`, `id_jasa`, `biaya_jasa`, `id_bahan`, `harga_bahan`, `jumlah`, `total`) VALUES
(125, 1, 'diproses', '2020-01-21', '2020-01-23', 1, 10000, 2, 50000, 3, 180000),
(126, 1, 'selesai', '2020-01-21', '2020-01-23', 1, 10000, 2, 50000, 3, 180000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_schedule`
--

CREATE TABLE `tbl_schedule` (
  `id` int(3) NOT NULL,
  `DestinationNumber` varchar(160) NOT NULL,
  `TextDecoded` varchar(15) NOT NULL,
  `CreatorID` varchar(5) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `nama_event` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_schedule`
--

INSERT INTO `tbl_schedule` (`id`, `DestinationNumber`, `TextDecoded`, `CreatorID`, `Time`, `nama_event`) VALUES
(5, '08989898989', 'Isi pesan ', 'Gammu', '2016-05-05 15:16:00', 'saga_event_1'),
(6, '082213148092', 'Tes SMS ', 'Gammu', '2016-05-09 10:05:00', 'saga_event_6');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `level_user` int(5) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` int(5) NOT NULL,
  `w_login` datetime NOT NULL,
  `photo` varchar(100) NOT NULL,
  `nohp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `pass`, `level_user`, `email`, `status`, `w_login`, `photo`, `nohp`) VALUES
(2, 'andez', '79803a0eaab30ae84b7b807bb46419f5', 1, 'andeztea@gmail.com', 1, '2015-07-03 10:27:24', 'assets/img/users_lock.png', '+08989899898');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `outbox`
--
ALTER TABLE `outbox`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `outbox_date` (`SendingDateTime`,`SendingTimeOut`),
  ADD KEY `outbox_sender` (`SenderID`);

--
-- Indexes for table `outbox_multipart`
--
ALTER TABLE `outbox_multipart`
  ADD PRIMARY KEY (`ID`,`SequencePosition`);

--
-- Indexes for table `pbk`
--
ALTER TABLE `pbk`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pbk_groups`
--
ALTER TABLE `pbk_groups`
  ADD PRIMARY KEY (`GroupID`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`IMEI`);

--
-- Indexes for table `sentitems`
--
ALTER TABLE `sentitems`
  ADD PRIMARY KEY (`ID`,`SequencePosition`),
  ADD KEY `sentitems_date` (`DeliveryDateTime`),
  ADD KEY `sentitems_tpmr` (`TPMR`),
  ADD KEY `sentitems_dest` (`DestinationNumber`),
  ADD KEY `sentitems_sender` (`SenderID`);

--
-- Indexes for table `tbl_autoreply`
--
ALTER TABLE `tbl_autoreply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_bahan`
--
ALTER TABLE `tbl_bahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_jasa`
--
ALTER TABLE `tbl_jasa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sablon`
--
ALTER TABLE `tbl_sablon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_bahan` (`id_bahan`),
  ADD KEY `id_jasa` (`id_jasa`);

--
-- Indexes for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=270;

--
-- AUTO_INCREMENT for table `outbox`
--
ALTER TABLE `outbox`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT for table `pbk`
--
ALTER TABLE `pbk`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `pbk_groups`
--
ALTER TABLE `pbk_groups`
  MODIFY `GroupID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_autoreply`
--
ALTER TABLE `tbl_autoreply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_bahan`
--
ALTER TABLE `tbl_bahan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_jasa`
--
ALTER TABLE `tbl_jasa`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_sablon`
--
ALTER TABLE `tbl_sablon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_sablon`
--
ALTER TABLE `tbl_sablon`
  ADD CONSTRAINT `tbl_sablon_ibfk_1` FOREIGN KEY (`id_bahan`) REFERENCES `tbl_bahan` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_sablon_ibfk_2` FOREIGN KEY (`id_jasa`) REFERENCES `tbl_jasa` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
