(function($) {

    $(document).ready(function(e) {


        var id_pb = 0;
        var main = "phonebook/pb.data.php";


        $("#data-pb").load(main);

        $('input:text[name=pencarian]').on('input',function(e){
            var v_cari = $('input:text[name=pencarian]').val();

            if(v_cari!="") {
                $.post(main, {cari: v_cari} ,function(data) {

                    $("#data-pb").html(data).show();
                });
            } else {

                $("#data-pb").load(main);
            }
        });


        $('.ubah,.tambah').live("click", function(){

            var url = "phonebook/pb.form.php";

            id_pb = this.id;

            if(id_pb != 0) {

                $("#myModalLabel").html("Ubah Data Kontak");
            } else {
                $("#myModalLabel").html("Tambah Data Kontak");
            }

            $.post(url, {id: id_pb} ,function(data) {
                $(".pbk").html(data).show();
            });
        });


        $('.hapus').live("click", function(){
            var url = "phonebook/pb.input.php";

            id_pb = this.id;

            var answer = confirm("Apakah anda ingin menghapus pb ini?");

            if (answer) {

                $.post(url, {hapus: id_pb} ,function() {

                    $("#data-pb").load(main);
                });
            }
        });

        $('.halaman').live("click", function(event){

            kd_hal = this.id;

            $.post(main, {halaman: kd_hal} ,function(data) {

                $("#data-pb").html(data).show();
            });
        });

        $("#simpan-pb").bind("click", function(event) {
            var url = "phonebook/pb.input.php";

            var vid_pb = $('input:text[name=id_pb]').val();

            var vname= $('input:text[name=name]').val();

            var vnomorHP= $('input:text[name=nomorHP]').val();

            var vgroup_id= $('select[name=group_id]').val();

            $.post(url, {id_pb: vid_pb, name: vname, nomorHP : vnomorHP, group_id : vgroup_id,
                id: id_pb},function() {
                $("#data-pb").load(main);
                $("#dialog-pb").modal('hide');
                $("#myModalLabel").html("Tambah Data Kontak");

            });
        });
    });
}) (jQuery);