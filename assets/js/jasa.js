(function($) {

    $(document).ready(function(e) {


        var id_pbk = 0;
        var main = "konfigurasi/jasa/jasa.data.php";


        $("#data-jasa").load(main);

        $('input:text[name=pencarian]').on('input',function(e){
            var v_cari = $('input:text[name=pencarian]').val();

            if(v_cari!="") {
                $.post(main, {cari: v_cari} ,function(data) {

                    $("#data-jasa").html(data).show();
                });
            } else {

                $("#data-jasa").load(main);
            }
        });


        $('.ubah,.tambah').live("click", function(){

            var url = "konfigurasi/jasa/jasa.form.php";

            id = this.id;

            if(id != 0) {

                $("#myModalLabel").html("Ubah Data Jasa");
            } else {
                $("#myModalLabel").html("Tambah Data Jasa");
            }

            $.post(url, {id: id} ,function(data) {
                $(".jasa").html(data).show();
            });
        });


        $('.hapus').live("click", function(){
            var url = "konfigurasi/jasa/jasa.input.php";

            id_jasa = this.id;

            var answer = confirm("Apakah anda ingin menghapus jasa ini?");

            if (answer) {

                $.post(url, {hapus: id_jasa} ,function() {

                    $("#data-jasa").load(main);
                });
            }
        });

        $('.halaman').live("click", function(event){

            kd_hal = this.id;

            $.post(main, {halaman: kd_hal} ,function(data) {

                $("#data-jasa").html(data).show();
            });
        });

        $("#simpan-jasa").bind("click", function(event) {
            var url = "konfigurasi/jasa/jasa.input.php";

            var vid = $('input:text[name=id]').val();

            var vnama= $('input:text[name=nama]').val();

            var vbiaya= $('input:text[name=biaya]').val();

            $.post(url, {id: vid, nama: vnama,
                biaya: vbiaya},function() {
                $("#data-jasa").load(main);
                $("#dialog-jasa").modal('hide');
                $("#myModalLabel").html("Tambah Data Jasa");

            });
        });
    });
}) (jQuery);