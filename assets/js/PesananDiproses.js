(function($) {

    $(document).ready(function(e) {

        var main = "pesanan/pesananDiproses/pesananDiproses.data.php";


        $("#data-pesanan-diproses").load(main);

        $('input:text[name=pencarian]').on('input',function(e){
            var v_cari = $('input:text[name=pencarian]').val();

            if(v_cari!="") {
                $.post(main, {cari: v_cari} ,function(data) {

                    $("#data-pesanan-diproses").html(data).show();
                });
            } else {

                $("#data-pesanan-diproses").load(main);
            }
        });

        $('.halaman').live("click", function(event){

            kd_hal = this.id;

            $.post(main, {halaman: kd_hal} ,function(data) {

                $("#data-pesanan-diproses").html(data).show();
            });
        });

        $('.ubah').live("click", function(){

            var url = "pesanan/pesananDiproses/pesananDiproses.form.php";

            id_sablon = this.id;

            $.post(url, {id: id_sablon} ,function(data) {
                $(".pesananDiproses").html(data).show();
            });
        });


        $('.hapus').live("click", function(){
            var url = "pesanan/pesananDiproses/pesananDiproses.input.php";

            id_group = this.id;

            var answer = confirm("Apakah anda ingin menghapus pesanan ini?");

            if (answer) {

                $.post(url, {hapus: id_group} ,function() {

                    $("#data-pesananDiproses").load(main);
                });
            }
        });

        $("#simpan-pesananDiproses").bind("click", function(event) {
            var url = "pesanan/pesananDiproses/pesananDiproses.input.php";

            var vid_sablon = $('input:text[name=id_sablon]').val();

            var vid_user= $('select[name=id_user]').val();

            var vid_bahan= $('select[name=id_bahan]').val();

            var vid_jasa= $('select[name=id_jasa]').val();

            var vjumlah= $('input:text[name=jumlah]').val();

            var vtanggal_selesai= $('input:text[name=tanggal_selesai]').val();

            var vstatus = $('select[name=status]').val();

            $.post(url, {id: vid_sablon, id_user: vid_user, id_bahan : vid_bahan, id_jasa : vid_jasa, jumlah : vjumlah, tanggal_selesai : vtanggal_selesai, status : vstatus
                },function() {
                $("#data-pesanan-diproses").load(main);
                $("#dialog-pesananDiproses").modal('hide');

            });
        });

    });
}) (jQuery);