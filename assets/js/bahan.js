(function($) {

    $(document).ready(function(e) {


        var id_pbk = 0;
        var main = "konfigurasi/bahan/bahan.data.php";


        $("#data-bahan").load(main);

        $('input:text[name=pencarian]').on('input',function(e){
            var v_cari = $('input:text[name=pencarian]').val();

            if(v_cari!="") {
                $.post(main, {cari: v_cari} ,function(data) {

                    $("#data-bahan").html(data).show();
                });
            } else {

                $("#data-bahan").load(main);
            }
        });


        $('.ubah,.tambah').live("click", function(){

            var url = "konfigurasi/bahan/bahan.form.php";

            id = this.id;

            if(id != 0) {

                $("#myModalLabel").html("Ubah Data Bahan");
            } else {
                $("#myModalLabel").html("Tambah Data Bahan");
            }

            $.post(url, {id: id} ,function(data) {
                $(".bahan").html(data).show();
            });
        });


        $('.hapus').live("click", function(){
            var url = "konfigurasi/bahan/bahan.input.php";

            id_bahan = this.id;

            var answer = confirm("Apakah anda ingin menghapus bahan ini?");

            if (answer) {

                $.post(url, {hapus: id_bahan} ,function() {

                    $("#data-bahan").load(main);
                });
            }
        });

        $('.halaman').live("click", function(event){

            kd_hal = this.id;

            $.post(main, {halaman: kd_hal} ,function(data) {

                $("#data-bahan").html(data).show();
            });
        });

        $("#simpan-bahan").bind("click", function(event) {
            var url = "konfigurasi/bahan/bahan.input.php";

            var vid = $('input:text[name=id]').val();

            var vnama= $('input:text[name=nama]').val();

            var vharga= $('input:text[name=harga]').val();

            $.post(url, {id: vid, nama: vnama,
                harga: vharga},function() {
                $("#data-bahan").load(main);
                $("#dialog-bahan").modal('hide');
                $("#myModalLabel").html("Tambah Data Bahan");

            });
        });
    });
}) (jQuery);