(function($) {

    $(document).ready(function(e) {

        var main = "pesanan/pesananSelesai/pesananSelesai.data.php";


        $("#data-pesanan-selesai").load(main);

        $('input:text[name=pencarian]').on('input',function(e){
            var v_cari = $('input:text[name=pencarian]').val();

            if(v_cari!="") {
                $.post(main, {cari: v_cari} ,function(data) {

                    $("#data-pesanan-selesai").html(data).show();
                });
            } else {

                $("#data-pesanan-selesai").load(main);
            }
        });

        $('.halaman').live("click", function(event){

            kd_hal = this.id;

            $.post(main, {halaman: kd_hal} ,function(data) {

                $("#data-pesanan-selesai").html(data).show();
            });
        });


    });
}) (jQuery);