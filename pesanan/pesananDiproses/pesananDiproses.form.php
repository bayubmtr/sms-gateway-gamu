<?php
session_start();
$sesi_username			= isset($_SESSION['username']) ? $_SESSION['username'] : NULL;
if ($sesi_username != NULL || !empty($sesi_username) ||$_SESSION['leveluser']=='1'  )

{

    include "../../config/koneksi.php";


    $id_sablon = $_POST['id'];

    $data = mysqli_fetch_array(mysqli_query($mysqli,"SELECT * FROM tbl_sablon WHERE id=".$id_sablon));


    if($id_sablon > 0) {
        $id_sablon  = $data['id'];
        $id	= $data['id'];
        $id_user	= $data['id_user'];
        $id_bahan	= $data['id_bahan'];
        $jumlah		= $data['jumlah'];
        $id_jasa	= $data['id_jasa'];
        $tanggal_selesai	= date("d-m-Y", strtotime($data['tanggal_selesai']));
        $status		= $data['status'];

    }

    ?>

    <form class="form-horizontal style-form" id="form-grp">

        <div class="form-group" hidden="hidden">
            <label class="col-sm-2 col-sm-2 control-label">id</label>
            <div class="col-sm-2">
                <input type="text" name="id_sablon" id="id_sablon" value="<?php echo $id_sablon ?>">
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Nama User</label>
            <div class="col-sm-5">
                <select class="form-control" name="id_user" placeholder="Isikan Nama Bahan">
                <?php
                $query = mysqli_query($mysqli,"SELECT * FROM pbk");
                while($data = mysqli_fetch_array($query)){
                ?>
                    <option <?php echo $data['ID'] == $id_user ? 'selected' : '' ?>  value="<?php echo $data['ID'] ?>"><?php echo $data['Name'] ?></option>
                <?php
                }
                ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Nama Bahan</label>
            <div class="col-sm-5">
                <select class="form-control" name="id_bahan" placeholder="Isikan Nama Bahan">
                <?php
                $query = mysqli_query($mysqli,"SELECT * FROM tbl_bahan");
                while($data = mysqli_fetch_array($query)){
                ?>
                    <option <?php echo $data['id'] == $id_bahan ? 'selected' : '' ?> value="<?php echo $data['id'] ?>"><?php echo $data['nama']; echo ' - '.$data['harga'] ?></option>
                <?php
                }
                ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Jumlah pesanan</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="jumlah" id="jumlah" value="<?php echo $jumlah ?>" placeholder="Isikan jumlah pesanan">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Nama Jasa</label>
            <div class="col-sm-5">
                <select class="form-control" name="id_jasa" placeholder="Isikan Nama Bahan">
                <?php
                $query = mysqli_query($mysqli,"SELECT * FROM tbl_jasa");
                while($data = mysqli_fetch_array($query)){
                ?>
                    <option <?php echo $data['id'] == $id_jasa ? 'selected' : '' ?> value="<?php echo $data['id'] ?>"><?php echo $data['nama']; echo ' - '.$data['biaya'] ?></option>
                <?php
                }
                ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Tanggal selesai</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="tanggal_selesai" value="<?php echo $tanggal_selesai ?>" id="tanggal_selesai" placeholder="format (tanggal-bulan-tahun)">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Status</label>
            <div class="col-sm-5">
                <select class="form-control" name="status" placeholder="Isikan Nama Bahan">
                    <option <?php echo $data['status'] == 'diproses' ? 'selected' : '' ?> value="diproses">Diproses</option>
                    <option <?php echo $data['status'] == 'selesai' ? 'selected' : '' ?> value="selesai">Selesai</option>
                </select>
            </div>
        </div>



    </form>

<?php
}else{
    session_destroy();
    header('Location:../index.php?status=Silahkan Login');
}
?>