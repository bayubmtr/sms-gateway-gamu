﻿<?php
error_reporting(0);
$sesi_username			= isset($_SESSION['username']) ? $_SESSION['username'] : NULL;

if ($sesi_username != NULL || !empty($sesi_username) ||$_SESSION['leveluser']=='1'||$_SESSION['leveluser']=='2'  )

{
    include "../config/koneksi.php";
?>
    <h3><i class="fa fa-angle-right"></i><?php echo $judul ?></h3>
    <div class="row mt">
        <div class="col-lg-12">
            <div class="content-panel">
                <section id="unseen">
                    <!-- textbox untuk pencarian -->
                    <form class="form-horizontal style-form" id="form-grp" method="post" action="pesanan/pesananBaru/PesananBaru.input.php">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nama User</label>
                            <div class="col-sm-5">
                                <select class="form-control" name="id_user" placeholder="Isikan Nama Bahan">
                                <?php
                                $query = mysqli_query($mysqli,"SELECT * FROM pbk");
                                while($data = mysqli_fetch_array($query)){
                                ?>
                                    <option value="<?php echo $data['ID'] ?>"><?php echo $data['Name'] ?></option>
                                <?php
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nama Bahan</label>
                            <div class="col-sm-5">
                                <select class="form-control" name="id_bahan" placeholder="Isikan Nama Bahan">
                                <?php
                                $query = mysqli_query($mysqli,"SELECT * FROM tbl_bahan");
                                while($data = mysqli_fetch_array($query)){
                                ?>
                                    <option value="<?php echo $data['id'] ?>"><?php echo $data['nama']; echo ' - '.$data['harga'] ?></option>
                                <?php
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Jumlah pesanan</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="Isikan jumlah pesanan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nama Jasa</label>
                            <div class="col-sm-5">
                                <select class="form-control" name="id_jasa" placeholder="Isikan Nama Bahan">
                                <?php
                                $query = mysqli_query($mysqli,"SELECT * FROM tbl_jasa");
                                while($data = mysqli_fetch_array($query)){
                                ?>
                                    <option value="<?php echo $data['id'] ?>"><?php echo $data['nama']; echo ' - '.$data['biaya'] ?></option>
                                <?php
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Tanggal selesai</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="tanggal_selesai" id="tanggal_selesai" placeholder="format (tanggal-bulan-tahun)">
                            </div>
                        </div>
                        <button id="simpan-pesanan" class="btn btn-success" data-dismiss="collapse" aria-hidden="true" >Simpan</button>

                    </form>
                </section>

            </div><!-- /content-panel -->
        </div><!-- /col-lg-4 -->
    </div><!-- /row -->
<?php
}else{
	  echo "<script>alert('Mohon Maaf anda tidak bisa akses halaman ini'); window.location = '../index.php'</script>";
}
?>
